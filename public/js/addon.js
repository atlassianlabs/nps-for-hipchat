function replaceSummary(data){
  $('.nps-goal-value').html(data.goal);
  $('.nps-current-value').html(data.nps);
}

function replaceComments(html) {
  var comments_container = $('.nps-comments-container');
  comments_container.append(html);
}

function onClickRow(data) {
  $('.nps-group').hide();
  $('.nps-focus').show();
  $('.back_arrow').click(function () {
    onClickBack();
  });
  $('.product_name').html("NPS");
  var dateString = data.date;
  $('.time_posted').html(dateString);
  $('.nps_comment_expanded').html('"' + data.comment + '"');
  
  var score = $('#score_badge').html(data.score);

  if(data.score_name=="good") {
    score.attr('class', 'score_badge aui-lozenge aui-lozenge-success');

  }
  else if(data.score_name=="normal") {
    score.attr('class', 'score_badge aui-lozenge aui-lozenge-current');

  }
  else if(data.score_name=="bad") {
    score.attr('class', 'score_badge aui-lozenge aui-lozenge-error');

  }

  $('#customer_gender_value').html(data.gender);
  $('#customer_age_value').html(data.age);
  $('#customer_income_value').html(data.income);
  $('#customer_country_value').html(data.country);


  $('.dropdown-li').unbind("click")
    .click((function (dat){
      return function () {
        sendItem(dat);
      }
    })(data));
}

function onClickBack() {
  $('.nps-focus').hide();
  $('.nps-group').show();
}

function createListHtml(data) {

  if(data.length == 0) {
    var noResponses = document.createElement("div");
    noResponses.setAttribute("class", "nps-sidebar-error-msg");
    noResponses.innerHTML = 'You haven\'t received any comments from your survey yet. Check back later!';
    return noResponses;
  }
  
  var listData = [];

  for (var i = 0; i < data.length; i++) {
    listData.push(processDataForList(data[i]));
  }

  var list  = document.createElement("ul");

  for (i = 0; i < listData.length; i++ ) {
    var row = document.createElement("li");
    row.setAttribute("class", "nps-feedback " + listData[i].score_name);

    var dat = listData[i];
    row.addEventListener("click", (function(dat){
      return function () {
        onClickRow(dat);
      }
    })(dat));

    var cellComment = document.createElement("div");
    cellComment.setAttribute("class", "nps-comment");
    cellComment.innerHTML = '"' + listData[i].comment + '"';
    
    var cellDate = document.createElement("div");
    cellDate.innerHTML = listData[i].date;
    cellDate.setAttribute("class", "nps-date");

    row.appendChild(cellComment);
    row.appendChild(cellDate);
    list.appendChild(row);
  }
  return list;
}

//Converts json data blob for an NPS comment for data ready to inject into list view
function processDataForList(data) {
  data.score_name = getRatingHex(data.score);
  return data;
}

//Returns hex color associated with each rating
function getRatingHex(score) {
  if (score > 8 ) {
    return 'good';
  }
  else if (score > 6 ) {
    return 'normal';
  }
  return 'bad';
}

function fetchNPSData() {
  $.ajax({
    url: '/nps_data',
    beforeSend: function (request) {
      request.setRequestHeader("X-acpt", ACPT);
    }
  }).done(function(data) {
    $(function() {
      console.log("addon.fetchNPSData");
      var commentHTML = createListHtml(data.commentValues);
      replaceComments(commentHTML);
      replaceSummary(data.npsValues);
    })
      
  }).fail(function() {
    replaceComments('Error retrieving list of responses. Please check the addon is correctly configured for your surveymonkey account.');
  });
}

function sendItem(data) {
  $.ajax({
    type: 'POST',
    url: '/post-comment',
    headers: {
      'X-acpt': ACPT
    },
    data: data
  })
}

function beginOAuth(surveyMonkeyURI) {
  var windowRef = window.open(surveyMonkeyURI);
  var timer = window.setInterval(function() {
    if (windowRef == null || windowRef.closed) {
      window.location.reload(true);
      clearInterval(timer);
    }
  }, 1000);
}

fetchNPSData();

AJS.$("#select-nps-filter").auiSelect2({
  minimumResultsForSearch: Infinity,
  width: '100%'
}).on("change", function(e) {
  if(e.val == 'all') {
    $('.nps-feedback.good').show();
    $('.nps-feedback.normal').show();
    $('.nps-feedback.bad').show();
  }
  else if(e.val == 'promoters') {
    $('.nps-feedback.good').show();
    $('.nps-feedback.normal').hide();
    $('.nps-feedback.bad').hide();
  }
  else if(e.val == 'passives') {
    $('.nps-feedback.good').hide();
    $('.nps-feedback.normal').show();
    $('.nps-feedback.bad').hide();
  }
  else if(e.val == 'detractors') {
    $('.nps-feedback.good').hide();
    $('.nps-feedback.normal').hide();
    $('.nps-feedback.bad').show();
  }
});


AJS.$("#nps-config-survey-names-select").auiSelect2({
  minimumResultsForSearch: Infinity,
  formatSelectionCssClass: function (data, container) { return "nps-select"; }
});


// This is how we receive data from the in-chat card link to display in the sidebar
function receiveParameters(parameters) {
  var data = processDataForList(parameters);
  onClickRow(data);
}
AP.register({
  "receive-parameters": receiveParameters
});

function displayConfigPage(authToken, surveyName, surveyList, goalNPS){
  if(authToken=='') {
    $('#nps-config-step-number-one').attr('src', window.location.origin + '/img/orange_1.svg');
    $('#nps-config-step-header-one').removeClass('nps-config-greyed-out');
    $('#nps-config-step-explanation-one').show();
    $('#nps-config-authenticate').show();
  }
  else if(surveyName=='') {
    $('#nps-config-step-number-one').attr('src', window.location.origin + '/img/green_1.svg');
    $('#nps-config-step-number-two').attr('src', window.location.origin + '/img/orange_2.svg');

    $('#nps-config-step-header-two').removeClass('nps-config-greyed-out');
    $('#nps-config-survey-names-form').show();
    if(surveyList && surveyList.length > 0) {
      $('#nps-config-no-surveys-found-option').remove();
      $('#nps-config-survey-name-save-button').removeAttr('disabled');
      surveyList.forEach(function(surveyName) {
        $('#nps-config-survey-names-select').append("<option>" + surveyName + "</option");
      });
    }
  }
  else {
    console.log(goalNPS);
    $('#nps-config-step-number-one').attr('src', window.location.origin + '/img/green_1.svg');
    $('#nps-config-step-number-two').attr('src', window.location.origin + '/img/green_2.svg');
    if(goalNPS && goalNPS!='') {
      $('#nps-config-step-number-three').attr('src', window.location.origin + '/img/green_3.svg');
    }
    else {
      $('#nps-config-step-number-three').attr('src', window.location.origin + '/img/orange_3.svg');
    }
    $('#nps-config-step-header-three').removeClass('nps-config-greyed-out');
    $('#nps-config-step-explanation-three').show();
    $('#nps-config-goal-form').show();
  }

}