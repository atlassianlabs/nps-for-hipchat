**NOTE:** This add-on does not work with the latest version of the SurveyMonkey API, so it has been retired. RIP buddy :(




# NPS for Hipchat

Listed on marketplace here: [https://marketplace.atlassian.com/plugins/hipchat-nps-add-on](https://marketplace.atlassian.com/plugins/hipchat-nps-add-on).

Usage guide here: [https://bitbucket.org/atlassianlabs/nps-for-hipchat/wiki/Usage%20guide](https://bitbucket.org/atlassianlabs/nps-for-hipchat/wiki/Usage%20guide).

This is a HipChat add-on for integrating a HipChat room with an NPS survey on SurveyMonkey, developed as part of Atlassian ShipIt 32.

# ACE

This add-on is built using Atlassian Connect Express (ACE) for Hipchat, which is an Atlassian-provided node.js framework.
See the documentation here: [https://bitbucket.org/hipchat/atlassian-connect-express-hipchat](https://bitbucket.org/hipchat/atlassian-connect-express-hipchat).
There's also some information in the documentation for non-HipChat Atlassian Connect Express that might be useful: [https://bitbucket.org/atlassian/atlassian-connect-express](https://bitbucket.org/atlassian/atlassian-connect-express).

# Project Structure

* `atlassian-connect.json` - the descriptor file. Defines to HipChat the add-on's capabilities
* `config.json` - a config file used by ACE
* `app.js` - main node script, provided by ACE. Performs set-up and runs the add-on
* `views` - handlebars templates for the config page and the sidebar view
* `routes/index.js` - node.js script handling routing. REST endpoints for the add-on server are defined here
* `lib` - node.js scripts handling custom logic
	* `card.js` - used to format feedback data into the card JSON expected by HipChat
	* `hipchat.js` - atlassian-provided libraries for HipChat
	* `nps.js` - NPS logic
	* `surveymonkey.js` - SurveyMonkey-specific logic
* `public` - client-side javascript and css
	* `addon.js` - client-side javascript for both the config page and the sidebar. This should be refactored into separate files at some stage

	
# Developing and hosting locally

Follow the ACE documentation above to expose your running app to the internet and install on HipChat. Note that the default port for the app is 8080, so you'll need to expose that port in particular.
You can change the port in `config.json`.
To get the add-on working locally with SurveyMonkey, you'll need to create a developer SurveyMonkey account to access the APIs. You'll then need to set some environment variables to match your account.

* `API_KEY` - found at [https://developer.surveymonkey.com/apps/mykeys](https://developer.surveymonkey.com/apps/mykeys)
* `CLIENT_SECRET` - found at [https://developer.surveymonkey.com/apps/mykeys](https://developer.surveymonkey.com/apps/mykeys)
* `CLIENT_ID` - your developer account username
* `REDIRECT_URI` - this is the endpoint that SurveyMonkey needs to hit as part of the oauth handshake. It's equal to your local base URL + '/surveymonkeyoauth2callback'. This value also needs to be registered with SurveyMonkey here: [https://developer.surveymonkey.com/apps/myapps](https://developer.surveymonkey.com/apps/myapps)

# JIRA project
We'll be setting up a JIRA project to track bugs and feature requests soon. Link to come!

# Contact
* ldenton@atlassian.com
* mbelton@atlassian.com
* jfurler@atlassian.com