var http = require('request');

module.exports = function (app, addon) {
  var hipchat = require('../lib/hipchat')(addon);
  var card = require('../lib/card')(addon);
  var surveymonkey = require('../lib/surveymonkey')(addon);
  var nps = require('../lib/nps')(addon, surveymonkey);

  var DELIMITER = '.';
  
  function generateIdentifier(req) {
    return req.clientInfo.clientKey.concat(DELIMITER).concat(req.clientInfo.roomId.toString());
  }
  
  // Root route. This route will serve the `addon.json` unless a homepage URL is
  // specified in `addon.json`.
  app.get('/',
    function(req, res) {
      // Use content-type negotiation to choose the best way to respond
      res.format({
        // If the request content-type is text-html, it will decide which to serve up
        'text/html': function () {
          res.redirect(addon.descriptor.links.homepage);
        },
        // This logic is here to make sure that the `addon.json` is always
        // served up when requested by the host
        'application/json': function () {
          res.redirect('/atlassian-connect.json');
        }
      });
    }
  );

  // This is an example route that's used by the default for the configuration page
  app.get('/config',
    // Authenticates the request using the JWT token in the request
    addon.authenticate(),
    function(req, res) {
      res.render('rip');
      // var key = generateIdentifier(req);
      // addon.settings.get('accessToken', key).then(function(token) {
      //   var surveyMonkeyURI = surveymonkey.getSurveyMonkeyURI(key);
      //   if(!token) {
      //     res.render('config', {
      //       surveyMonkeyURI: surveyMonkeyURI
      //     });
      //     return;
      //   }
      //   surveymonkey.getSurveyList(key).then(function(surveyList) {
      //     addon.settings.get('surveyName', key).then(function (surveyName) {
      //       addon.settings.get('goalNPS', key).then(function (goal) {
      //         res.render('config', {
      //           surveyMonkeyURI: surveyMonkeyURI,
      //           accessToken: token,
      //           surveyName: surveyName,
      //           goalNPS: goal,
      //           surveyList: surveyList.data.surveys,
      //           badGoal: req.query.badGoal,
      //           goodGoal: req.query.goodGoal,
      //           badSurvey: req.query.badSurvey
      //         });
      //       });
      //     });
      //   });
      //
      // });

    }
  );

  // Notify the room that the add-on was installed
  addon.on('installed', function(clientKey, clientInfo, req){
    hipchat.sendMessage(clientInfo, req.body.roomId, 'The ' + addon.descriptor.name + ' add-on has been installed in this room');
  });

  // Clean up clients when uninstalled
  addon.on('uninstalled', function (id) {
    addon.settings.client.keys(id + ':*', function (err, rep) {
      rep.forEach(function (k) {
        addon.logger.info('Removing key:', k);
        addon.settings.client.del(k);
      });
    });
  });

  // This route provides the content for the glance
  app.get('/glance',
    addon.authenticate(),
    function (req, res) {
      var key = generateIdentifier(req);
      nps.getGlanceData(key).then(function (data) {
        res.header("Access-Control-Allow-Origin", "*");
        res.send(data);
      }, function(error) {
        res.send();
      });
    }
  );

    app.get('/rip', function(req, res) {
        res.render('rip');
    });

  app.get('/sidebar',
    addon.authenticate(),
    function (req, res) {
      // var key = generateIdentifier(req);
      // res.render('sidebar', {
      //   key: key
      // });
      res.render('rip');
    }
  );

  app.get('/nps_data',
    addon.authenticate(),
    function (req, res) {
      console.log("index.get_nps_data");
      var key = generateIdentifier(req);
      nps.refreshNPSData(key)
        .then(function (data) {
          res.send(data);
        }, function(error) {
          console.log('sending error!');
          res.status(500).send(error);
      });
    }
  );

  app.post('/post-comment',
    addon.authenticate(),
    function (req, res) {
      var cardContents = card.getJson(req.body);
      hipchat.sendCard(req.clientInfo, req.clientInfo.roomId, cardContents, {options: {color: 'gray'}}).then(function(response) {
        res.status(200).send("Comment card posted to HipChat room " + req.clientInfo.roomId);
      });
    }
  );

  app.get('/refresh_nps_data',
    function (req, res) {
      var key = generateIdentifier(req);
      nps.refreshNPSData(key).then(function () {
        res.send('Done!');
      })
    }
  );

  app.get('/healthcheck',
    function (req, res) {
      res.send("ok!");
    }
  );

  app.get('/surveymonkeyoauth2callback',
    function(req, res) {
      surveymonkey.handleRedirect(req.url, req.query.clientKey, req.query.code, req.query.error, req.query.error_description);
      console.log('Authenticated with SurveyMonkey. Closing window');
      res.send('<script>setTimeout(function () {window.close();}, 2000);</script>Processing...');
    }
  );

  app.post('/submit-survey-name',
    addon.authenticate(),
    function(req, res) {
      var key = generateIdentifier(req);
      var surveyName = req.body['surveyname'];
      surveymonkey.populateSurveyData(key, surveyName).then(function(){
        addon.settings.set('surveyName', surveyName, generateIdentifier(req)).then(function(){
          res.redirect('/config?signed_request=' + req.query['signed_request']);
        })
      }, function(error){
        console.log('Selected survey did not validate');
        res.redirect('/config?signed_request=' + req.query['signed_request'] + '&badSurvey=true');
      });
    }
  );

  app.post('/submit-survey-goal',
    addon.authenticate(),
    function(req, res) {
      var badGoal = '';
      var goodGoal = '';
      if(req.body['save']) {
        var goal = req.body['goal'];
        if (!isNaN(parseFloat(goal)) && isFinite(goal) && goal >= -100 && goal <= 100) {
          addon.settings.set('goalNPS', req.body['goal'], generateIdentifier(req));
          goodGoal = '&goodGoal=true';
        }
        else {
          badGoal = '&badGoal=' + goal;
        }
      }
      
      else if(req.body['reconfigure']) {
        addon.settings.del('accessToken', generateIdentifier(req));
        addon.settings.del('surveyName', generateIdentifier(req));
      }
      
      res.redirect('/config?signed_request=' + req.query['signed_request'] + badGoal + goodGoal);
    }
  );
  
};