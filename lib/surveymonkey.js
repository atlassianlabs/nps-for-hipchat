var request = require('request');
var SurveyMonkeyAPI = require('surveymonkey').SurveyMonkeyAPI;
var _ = require('underscore');
var RSVP = require('rsvp');
var moment = require('moment');

// The following values are associated with a SurveyMonkey developer account, stored as env variables
var API_KEY = process.env.API_KEY;
var CLIENT_SECRET = process.env.CLIENT_SECRET;
var CLIENT_ID = process.env.CLIENT_ID;
var REDIRECT_URI = process.env.REDIRECT_URI;

var surveyData = {};

module.exports = function(addon) {
  
  var getSurveyList = function (key) {
    return new RSVP.Promise(function(resolve, reject) {
      addon.settings.get('accessToken', key).then(function(authToken) {
        var api = new SurveyMonkeyAPI(API_KEY, authToken, {secure: true});
        api.getSurveyList({
          "fields": ["title", "analysis_url", "date_created", "date_modified"],
          "order_asc": false
        }, function (error, data) {
          if (error) {
            console.log(error.message);
            reject(error);
          }
          else {
            resolve(data);
          }
        });
      });
    });
  };
  
  var populateSurveyData = function (key, surveyName) {
    return new RSVP.Promise(function(resolve, reject) {
      addon.settings.get('accessToken', key).then(function(authToken) {
        surveyData[key] = {};
        getSurveyList(key).then(function(data) {
          _.each(data.data.surveys, function (survey) {
            if (survey.title.toUpperCase() == surveyName.toUpperCase()) {
              surveyData[key].surveyId = survey.survey_id;
              surveyData[key].surveyName = survey.title;
            }
          });
          if(!surveyData[key].surveyId) {
            var errormsg = 'Could not find survey with provided name';
            console.log(errormsg);
            reject(errormsg);
            return;
          }

          var api = new SurveyMonkeyAPI(API_KEY, authToken, {secure: true});

          var scoreAnswerIds = {};
          var genderAnswerIds = {};
          var ageAnswerIds = {};
          var incomeAnswerIds = {};
          var questionIds = {};
          var commentFieldExists = false;
          api.getSurveyDetails({"survey_id": surveyData[key].surveyId}, function (error, data) {
            try {
              
              _.each(data.data.pages, function (page) {
                _.each(page.questions, function (question) {
                  if (question.type.subtype == 'rating') {
                    _.each(question.answers, function (answer) {
                      if (answer.type == 'col') {
                        scoreAnswerIds[answer.answer_id] = answer.position - 1;
                      }
                    });
                    surveyData[key].scoreAnswerIds = scoreAnswerIds;
                    questionIds[question.question_id] = 'score';
                    
                  }
                  
                  else if (question.type.subtype == 'essay') {
                    questionIds[question.question_id] = 'comment';
                    commentFieldExists = true;
                    
                  }
                  
                  else if (question.heading.toLowerCase().indexOf(" gender") > -1) {
                    _.each(question.answers, function (answer) {
                      genderAnswerIds[answer.answer_id] = answer.text;
                    });
                    surveyData[key].genderAnswerIds = genderAnswerIds;
                    questionIds[question.question_id] = 'gender';


                  }

                  else if (question.heading.toLowerCase().indexOf(" age") > -1) {
                    _.each(question.answers, function (answer) {
                      ageAnswerIds[answer.answer_id] = answer.text;
                    });
                    surveyData[key].ageAnswerIds = ageAnswerIds;
                    questionIds[question.question_id] = 'age';


                  }

                  else if (question.heading.toLowerCase().indexOf(" income") > -1) {
                    _.each(question.answers, function (answer) {
                      incomeAnswerIds[answer.answer_id] = answer.text;
                    });
                    surveyData[key].incomeAnswerIds = incomeAnswerIds;
                    questionIds[question.question_id] = 'income';


                  }

                  else if (question.heading.toLowerCase().indexOf(" country") > -1) {
                    _.each(question.answers, function (answer) {
                      if (answer.text.toLowerCase() === "united states") {
                        surveyData[key].USAnswerId = answer.answer_id;
                      }
                      questionIds[question.question_id] = 'country';


                    });
                  }
                  
                });
                
              });
              if (Object.keys(scoreAnswerIds).length != 11 || !commentFieldExists) {
                reject('Survey did not meet minimum requirements for an NPS survey');
              }
              surveyData[key].questionIds = questionIds;
              console.log('finished populating the survey info');
              resolve(key);
            }
            catch(err) {
              reject('Error reading survey details');
            }
          });
        });
      });
    });
    
  };
  
  
  var getData = function(key) {
    return new RSVP.Promise(function(resolve, reject) {
      console.log('starting to fetch the data');
      var respondentList = [];
      var respondentDates = {};
      addon.settings.get('accessToken', key).then(function (authToken) {
        var api = new SurveyMonkeyAPI(API_KEY, authToken, {secure: true});
        var responses = [];
        api.getRespondentList({
          "survey_id": surveyData[key].surveyId,
          "fields": ["date_modified"]
        }, function (error, data) {
          if (error) {
            console.log(error.message);
          }
          else if (data.data.respondents.length == 0) {
            resolve(responses);
          }
          else {
            data.data.respondents.forEach(function (respondent) {
              respondentList.push(respondent.respondent_id);
              respondentDates[respondent.respondent_id] = respondent.date_modified;
            });
            api.getResponses({
              "survey_id": surveyData[key].surveyId,
              "respondent_ids": respondentList
            }, function (error, data) {
              if (error) {
                console.log(error.message);
              }
              else {
                try {
                  data.data.forEach(function (responseData) {
                    var response = {};


                    var date = moment(respondentDates[responseData.respondent_id]);
                    response.date = date.format('D MMMM YYYY');

                    noValueString = "n/a";
                    response.gender = noValueString;
                    response.age = noValueString;
                    response.income = noValueString;
                    response.country = noValueString;
                    response.comment = noValueString;

                    // There will always be a score and date, but all other fields could be left blank
                    responseData.questions.forEach(function (question) {
                      var questionType = surveyData[key].questionIds[question.question_id];
                      if (questionType === 'score') {
                        response.score = surveyData[key].scoreAnswerIds[question.answers[0].col];
                      }
                      else if (questionType === 'comment') {
                        response.comment = question.answers[0].text;
                      }
                      else if (questionType === 'gender') {
                        response.gender = surveyData[key].genderAnswerIds[question.answers[0].row];
                      }
                      else if (questionType === 'age') {
                        response.age = surveyData[key].ageAnswerIds[question.answers[0].row];
                      }
                      else if (questionType === 'income') {
                        response.income = surveyData[key].incomeAnswerIds[question.answers[0].row];
                      }
                      else if (questionType === 'country') {
                        if (question.answers[0].row === surveyData[key].USAnswerId) {
                          response.country = 'United States';
                        }
                        else {
                          response.country = question.answers[0].text;
                        }
                      }
                    });

                    responses.push(response);
                  });
                  resolve(responses);
                }
                catch(err) {
                  reject('Response data didn\'t match our expectations. Please ensure your survey follows the SurveyMonkey NPS template.');
                }
              }
            });
          }
        });
      });
    });
    
  };
  
  return {
    
    getRedirectURIWithClientKey: function(key) {
      return REDIRECT_URI + "?clientKey=" + key;
    },
    
    getSurveyMonkeyURI: function(key) {
      return "https://surveymonkey.com/user/oauth/authorize?response_type=code&redirect_uri=" +
        encodeURIComponent(this.getRedirectURIWithClientKey(key)) +
        "&client_id=" + CLIENT_ID + "&api_key=" + API_KEY;
    },
    
    handleRedirect: function (url, key, code, error, error_description) {
      if (error) {
        console.log("error!" + error_description);
      }
      var redirectURI = this.getRedirectURIWithClientKey(key); // this has to be the unencoded value!
      request({
        url: "https://api.surveymonkey.net/oauth/token?api_key=" + API_KEY,
        method: 'POST',
        form: {
          client_secret: CLIENT_SECRET,
          code: code,
          redirect_uri: redirectURI,
          grant_type: "authorization_code",
          client_id: CLIENT_ID
        }
      }, function (error, response, body) {
        if (error) {
          console.log(error);
        }
        else {
          var accessToken = JSON.parse(body).access_token;
          if (accessToken == null) {
            console.log("Access token was not able to be stored. SurveyMonkey response: " + body);
          }
          else {
            addon.settings.set('accessToken', accessToken, key);
          }
        }
      });
    },
    
    populateSurveyData: populateSurveyData,
    getData: getData,
    getSurveyList: getSurveyList,
    
    checkSurveyInfoExistsAndGetResponses: function(key) {
      return new RSVP.Promise(function(resolve) {
        addon.settings.get('surveyName', key).then(function(surveyName) {
          if (surveyData[key] && surveyData[key].surveyName == surveyName) {
            console.log('We already have the survey data, don\'t need to fetch again');
            getData(key).then(function(data){
              resolve(data);
            });
          }
          else {
            console.log('We haven\'t seen this survey before, fetching its data');
            populateSurveyData(key, surveyName).then(function(key){
              getData(key).then(function(data){
                resolve(data);                
              }, function(error) {
                resolve(error);
              })
            }, function(error) {
              resolve(error);
            })
          }
        });
      });
    }
  }
};