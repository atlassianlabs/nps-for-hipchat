var RSVP = require('rsvp');
var _ = require('underscore');

module.exports = function (addon, source) {

  var ICON_PATH_GOOD = "/img/good_icon.svg";
  var ICON_PATH_BAD = "/img/bad_icon.svg";

  var BOTTOM_PASSIVE_SCORE = 7;
  var TOP_PASSIVE_SCORE = 8;

  var npsData = {};

  function refreshNPSData(key) {
    console.log("nps.refreshNPSData");
    return new RSVP.Promise(function (resolve, reject) {
      addon.settings.get('goalNPS', key).then(function(goalNPS) {
        source.checkSurveyInfoExistsAndGetResponses(key).then(function (data) {
          if (!(data instanceof Object)) {
            console.log('Error fetching nps data');
            reject(data);
          }
          else {
            var npsRecordsWithComments = getNPSRecordsWithComments(data);
            var npsSummary = getNPSSummary(data, parseInt(goalNPS));
            npsData = {
              npsValues: npsSummary,
              commentValues: npsRecordsWithComments
            };
            resolve(npsData);
          }
        });
      });
    });
  }

  function getNPSRecordsWithComments(data) {
    console.log("nps.getNPSRecordsWithComments");
    return data.filter(function (x) {
      return (x.comment != null);
    });
  }

  function getNPSSummary(data, goalNPS) {
    console.log("nps.getNPSSummary");
    var total = data.length;
    var promoters = data.filter(function (x) {
      return x.score > TOP_PASSIVE_SCORE;
    }).length;
    var detractors = data.filter(function (x) {
      return x.score < BOTTOM_PASSIVE_SCORE;
    }).length;
    var npsValue = Math.round(((promoters - detractors) / total) * 100.0);

    return {
      nps: npsValue,
      goal: goalNPS
    }
  }


  return {
    refreshNPSData: refreshNPSData,

    getNPSData: function (key) {
      console.log("nps.getNPSData");
      return new RSVP.Promise(function (resolve) {
        refreshNPSData(key).then(function () {
          resolve(npsData)
        });
      })
    },

    getGlanceData: function (key) {
      console.log("nps.getGlanceData");
      return new RSVP.Promise(function (resolve, reject) {
        addon.settings.get('goalNPS', key).then(function (goal) {
          addon.settings.get('surveyName', key).then(function (surveyName) {
            var payload = {
              "label": {
                "type": "html",
                "value": "NPS"
              }
            };
            if (goal && goal != '' && surveyName && surveyName != '') {
              refreshNPSData(key).then(function () {
                var icon_path;
                if (npsData.npsValues.nps >= goal || !npsData.npsValues.nps) {
                  icon_path = ICON_PATH_GOOD;
                }
                else {
                  icon_path = ICON_PATH_BAD;
                }
                payload["status"] = {
                  "type": "icon",
                  "value": {
                    "url": addon.config.localBaseUrl() + icon_path,
                    "url@2x": addon.config.localBaseUrl() + icon_path
                  }
                };
                resolve(payload);
              });
            }
            else {
              resolve(payload);
            }
          });
        });
      });
    }
  }
};
