module.exports = function(addon) {
    return {
        getJson: function (data) {
            var scoreLozengeStyle;
            if (data.score < 7)
            {
                scoreLozengeStyle = "lozenge-error";
            }
            else if (data.score > 8)
            {
                scoreLozengeStyle = "lozenge-success";
            }
            else
            {
                scoreLozengeStyle = "lozenge-current";
            }

            var logo = addon.config.localBaseUrl() + "/img/glance_icon_big.svg";

            var commentWithoutQuotes = data.comment.replace(/'/g, "").replace(/"/g, "");
            var countryWithoutQuotes = data.country.replace(/'/g, "").replace(/"/g, "");

            var optionsObject = "{" +
                "\"parameters\": {" +
                    "\"comment\": \"" + commentWithoutQuotes + "\"," +
                    "\"date\": \"" + data.date + "\"," +
                    "\"score\": \"" + data.score + "\"," +
                    "\"age\": \"" + data.age + "\"," +
                    "\"gender\": \"" + data.gender + "\"," +
                    "\"income\": \"" + data.income + "\"," +
                    "\"country\": \"" + countryWithoutQuotes + "\"" +
                "}" +
            "}";
            
            return {
                "style":"application",
                "description": {
                    "format": "html",
                    "value": "<a href='#' data-target='hipchat-nps-add-on:nps.webpanel.nps' " +
                                "data-target-options='" + optionsObject + "'\><i>" +
                                '\"' + data.comment + '\"' + "</i></a>"
                },
                "title":"NPS",
                "attributes":[
                    {
                        "value":{
                            "label":data.score,
                            "style":scoreLozengeStyle
                        },
                        "label":"Score"
                    }
                ],
                "id":data.comment,
                "icon":{
                    "url":logo
                }
            };
        }
    }
    
};
